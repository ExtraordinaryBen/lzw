#include "seq.h"

///
/// Constructor for Bits.
///
Seq* Seq_Create(unsigned char firstChar)
{
	Seq *seq = (Seq *)malloc(sizeof (Seq));
	
	seq->length = 1;
	
	seq->string = (unsigned char *)malloc(sizeof (unsigned char));
	
	seq->string[0] = firstChar;
	
	return seq;
}

///
/// Constructor for Seq with specific length/size.
///
Seq* Seq_Create_Size(unsigned int size)
{
	Seq *seq = (Seq *)malloc(sizeof (Seq));
	seq->length = 0;
	seq->string = (unsigned char *)malloc(size * sizeof (unsigned char));
	
	return seq;
}

///
/// Appends an additional byte to an existing sequence.
///
void Seq_Append(Seq *seq, unsigned char value)
{
	seq->string = (unsigned char *) realloc(seq->string, ++seq->length);
	
	seq->string[seq->length - 1] = value;
	
}

///
/// Makes a duplicate of seqA and stores it at seqB.
///
void Seq_Duplicate(Seq * seqA, Seq **seqB)
{
	if((*seqB) != NULL)
		Seq_Destroy((*seqB));
	
	(*seqB) = Seq_Create_Size(seqA->length);
	
	memcpy((*seqB)->string, seqA->string, seqA->length);
	
	(*seqB)->length = seqA->length;
	
}

///
/// Outputs the sequence seq to fd.
///
void Seq_Output(Seq* seq, FILE* fd)
{
	for(int i = 0; i < seq->length; i++)
		fputc(seq->string[i], fd);
}

///
/// Returns the first byte of a sequence.
///
unsigned char Seq_First_Char(Seq *seq)
{
	return seq->string[0];
}

///
/// Evaluates if two sequences are identical.
///
bool Seq_Are_Equal(Seq *seqA, Seq *seqB)
{
	if(seqA->length == seqB->length) {
		for(int i = 0; i < seqA->length; i++) {
			if(seqA->string[i] != seqB->string[i])
				return false;
		}
		return true;
	}
	else
		return false;
}

///
/// Destructor for Seq.
///
void Seq_Destroy(Seq *seq)
{
	if(seq != NULL) {
		free(seq->string);
		free(seq);
	}
}

///
/// Computes a hash for the sequence using a modified version of
/// the Bernstein Hash algorithm.
///
unsigned int Seq_Compute_Hash(Seq *seq, unsigned int modulus)
{
	unsigned int hash = 0;
	for (int i = 0; i < seq->length; i++)
			hash = 33 * hash ^ seq->string[i];

	return hash % modulus;
	
}
