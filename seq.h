#ifndef SEQ_H_
#define SEQ_H_

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <assert.h>
#include <string.h>

typedef struct Seq {
	unsigned int length;
	unsigned char *string;

} Seq; 

Seq* Seq_Create(unsigned char firstChar);

Seq* Seq_Create_Size(unsigned int size);

void Seq_Append(Seq*, unsigned char);

void Seq_Duplicate(Seq* seqA, Seq** seqB);

void Seq_Output(Seq* seq, FILE* fd);

void Seq_fPrintf(Seq*, FILE* fd);

unsigned char Seq_First_Char(Seq*);

bool Seq_Are_Equal(Seq*, Seq*);

void Seq_Destroy(Seq*);

unsigned int Seq_Compute_Hash(Seq *seq, unsigned int modulus);

#endif
