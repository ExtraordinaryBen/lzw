#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#include "lzw.h"

void Help_Message(char **);
void Set_FD(FILE **fd, int argc, char **argv, char *mode, int i);

int main(int argc, char *argv[])
{
	FILE *inputFD = stdin;
	FILE *outputFD = stdout;
	bool encode = false;
	bool decode = false;
	
	/* Process command arguments */
	for(unsigned int i = 0; i < argc; i++) {
		//fprintf(stdout, "Argument %d: %s\n", i, argv[i]);
		
		//Help message: default
		if(argc == 1 || (argc > 1 && strcmp(argv[1], "help") == 0)) {
			break;
		}
		else {
			if(strcmp(argv[i], "-encode") == 0)
				encode = true;
			else if(strcmp(argv[i], "-decode") == 0)
				decode = true;
			else if(strcmp(argv[i], "-input") == 0)
				Set_FD(&inputFD, argc, argv, "r", i);
			else if(strcmp(argv[i], "-output") == 0)		
				Set_FD(&outputFD, argc, argv, "w", i);
		}
	}
	
	if(encode != decode) {
		if(encode)
			LZW_Encode(BIT_WIDTH, inputFD, outputFD);
		else
			LZW_Decode(BIT_WIDTH, inputFD, outputFD);
	}
	else {
		if(encode && decode)
			fprintf(stderr, "%s error: cannot encode and decode at the same time!\n\n", argv[0]);
		else
			fprintf(stderr, "%s error: -encode or -decode not specified.\n\n", argv[0]);
			
		Help_Message(argv);
	}

	fclose(inputFD);
	fclose(outputFD);
	
	return 0;
}

void Help_Message(char **argv)
{
	fprintf(stderr, 
		"Usage:\n%s (-encode/-decode) -input FILE1 -output FILE2\n"
		"\t-encode: Compresses FILE1 and stores the results to FILE2\n"
		"\t-decode: Decompresses FILE1 and stores the results to FILE2\n\n"
		"\tIf -input or -output are not given, standard input and\n"
		"\tstandard output will used respectively.\n\n"
		"\tNote: -encode and -decode cannot be specified at the same time.\n\n",
		argv[0]
	);
	
}

void Set_FD(FILE **fd, int argc, char **argv, char *mode, int i)
{
	if(i + 1 >= argc) {
		fprintf(stderr, "%s error: missing argument for %s\n", argv[0], argv[i]);
		exit(1);
	}
	
	(*fd) = fopen(argv[i+1], mode);
	
	if((*fd) == NULL) {
		fprintf(stderr, "%s error: %s : %s\n", argv[0], strerror(errno), argv[i+1]);
		exit(1);
	}
	
}
