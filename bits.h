#ifndef BITS_H
#define BITS_H

#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>

#define BIT_WIDTH 16
#define BIT_W_MAX 0x10000

typedef struct Bits {
		unsigned int extraBits;
		unsigned int extraCount;
		unsigned int bits;
		
		FILE *fd;
		
} Bits;

Bits* Bits_Create(FILE *fd);

void Bits_Destory(Bits *bits);

bool Bits_Read(Bits* bits, unsigned int nBits, unsigned int *destination);

bool Bits_Write(Bits* bits, unsigned int nBits, unsigned int source);

bool Bits_Flush(Bits* bits);

#endif
