#ifndef DICT_H
#define DICT_H

#include <assert.h>


#include "seq.h"

#define MAX_HASH_SIZE 999983
#define INITIAL_CODE 256

typedef struct DictNode {
	Seq *key;		/* Sequence of characters */
	unsigned int code;
	struct DictNode *next;
	
} DictNode;

typedef struct _dict {
	unsigned int hashSize;
	DictNode **entries;
	
} Dict;

Dict* Dict_Create_Dictionary();

bool Dict_Find(Dict *dict, Seq *s,  DictNode **entry);

void Dict_Insert_Pair(Dict *dict, DictNode *pair, unsigned int index);

DictNode* Dict_Create_Node(Seq*, unsigned int);

void Dict_Destroy(Dict*);
void Dict_Destroy_Nodes(DictNode*);


#endif
