#include "bits.h"

///
/// Constructor for Bits.
///
Bits* Bits_Create(FILE *fd)
{
	Bits *thing = (Bits *)malloc(sizeof (Bits));
	
	thing->fd = fd;
	thing->extraBits = 0;
	thing->extraCount = 0;
	
	return thing;
}

///
/// Destructor for Bits.
///
void Bits_Destory(Bits *bits)
{
	free(bits);
}

///
/// Reads the entry code per byte.
/// TO-DO: re-impliment for nBits bit width.
///
bool Bits_Read(Bits* bits, unsigned int nBits, unsigned int *destination)
{	
	unsigned char msByte = fgetc(bits->fd);
	
	if(feof(bits->fd))
		return false;
	
	unsigned char lsByte = fgetc(bits->fd);
	
	(*destination) = (msByte << 8);
	(*destination) += lsByte;
	
	return true;
}

///
/// Writes the entry code per byte.
/// TO-DO: re-impliment for nBits bit width.
///
bool Bits_Write(Bits* bits, unsigned int nBits, unsigned int source)
{
	fputc( ((source >> 8) & 0xFF), bits->fd );
	fputc( (source & 0xFF), bits->fd );
	
	return true;
}

///
/// To be implemented when revised for variable bit width.
///
bool Bits_Flush(Bits* bits)
{

	return false;
}
