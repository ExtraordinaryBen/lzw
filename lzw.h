#ifndef LZW_H
#define LZW_H

#include <unistd.h>
#include <stdio.h>
#include <assert.h>

#include "bits.h"
#include "seq.h"
#include "dict.h"

void LZW_Encode(unsigned int maxBits, FILE *inputFD, FILE *outputFD);

void LZW_Decode(unsigned int maxBits, FILE *inputFD, FILE *outputFD);


#endif
