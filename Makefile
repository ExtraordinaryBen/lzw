#makefile
CFLAGS=-std=gnu99 -g -Wall
 
main: bits.o seq.o dict.o lzw.o main.o 
	gcc bits.o seq.o dict.o lzw.o main.o -o lzw

seq.o: seq.h seq.c
	gcc -c $(CFLAGS) seq.h seq.c

dict.o: seq.h dict.h dict.c
	gcc -c $(CFLAGS) seq.h dict.h dict.c

bits.o: bits.h bits.c
	gcc -c $(CFLAGS) bits.h bits.c

lzw.o:  bits.h seq.h dict.h lzw.h lzw.c
	gcc -c $(CFLAGS) bits.h seq.h dict.h lzw.h lzw.c

main.o: lzw.h main.c 
	gcc -c $(CFLAGS) lzw.h main.c

clean:
	rm -f lzw *.o *.gch
