#include "lzw.h"

///
/// Takes data from inputFD and sends a LZW encoded version to outputFD.
///
void LZW_Encode(unsigned int maxBits, FILE *inputFD, FILE *outputFD)
{
	unsigned int nextCode = INITIAL_CODE;
	unsigned char byte = fgetc(inputFD);
	int c = fgetc(inputFD);
	
	Bits *outputBits = Bits_Create(outputFD);
	Dict *dict = Dict_Create_Dictionary();
	Seq *w = Seq_Create(byte);
	Seq *x = NULL;
	DictNode *entry = NULL;
	
	while(c != EOF) {
		//printf("Read character: '%c'\n", c);
		Seq_Duplicate(w, &x);
		Seq_Append(x, c);
		//printf("Sequence x: '%s'\n", x->string);
		
		if(Dict_Find(dict, x, &entry)) {
			//printf("x exists: '%s', Code: %d\n\n", entry->key->string, entry->code);
			Seq_Duplicate(x, &w);
			
		}
		else {
			Dict_Find(dict, w, &entry);
			
			//output assigned code of w.
			//printf("Write w Sequence: '%s', Code: %d\n\n", entry->key->string, entry->code);
			Bits_Write(outputBits, BIT_WIDTH, entry->code);
			
			if(nextCode < BIT_W_MAX) {
				Seq *new = NULL;
				Seq_Duplicate(x, &new);
				Dict_Insert_Pair(dict, Dict_Create_Node(new, nextCode), Seq_Compute_Hash(new, MAX_HASH_SIZE));
				
				//printf("New Sequence: '%s', Code: %d\n\n", new->string, nextCode);
				nextCode += 1;
			}
			
			Seq_Destroy(w);
			w = Seq_Create(c);
		}
		
		c = fgetc(inputFD);
	}
	
	Dict_Find(dict, w, &entry);
	
	//output assigned code of w.
	//printf("Sequence: '%s', Code: %d\n\n", entry->key->string, entry->code);
	Bits_Write(outputBits, BIT_WIDTH, entry->code);

	//clean-up
	Dict_Destroy(dict);
	Bits_Destory(outputBits);
	Seq_Destroy(w);
	Seq_Destroy(x);
	
}

///
/// Takes LZW encoded data from inputFD and sends a decoded version to outputFD.
///
void LZW_Decode(unsigned int maxBits, FILE *inputFD, FILE *outputFD)
{
	unsigned int nextCode = INITIAL_CODE;
	unsigned int previousCode = 0;
	
	Seq **table = (Seq **)calloc(BIT_W_MAX, sizeof (Seq *));
	Bits *inputBits = Bits_Create(inputFD);
	
	for(int i=0; i < INITIAL_CODE; i++) {
		table[i] = Seq_Create(i);
	}
	
	Bits_Read(inputBits, maxBits, &previousCode);
	assert(previousCode < INITIAL_CODE);
	Seq_Output(table[previousCode], outputFD);
		
	unsigned char c;
	unsigned int currentCode;
	
	while( Bits_Read(inputBits, maxBits, &currentCode) ) {
		//fprintf(stderr, "Current Code: %04x\n", currentCode);
		
		if(table[currentCode] != NULL)
			c = Seq_First_Char(table[currentCode]);
		else
			c = Seq_First_Char(table[previousCode]);
		
		if(nextCode < BIT_W_MAX) {
			Seq *w = NULL;
			Seq_Duplicate(table[previousCode], &w);
			Seq_Append(w, c);
			
			table[nextCode] = w;
			//fprintf(stderr, "Code: %04x, string: '%s'\n\n", nextCode, table[nextCode]->string);
			
			nextCode += 1;
		}
		
		Seq_Output(table[currentCode], outputFD);
		//Seq_Output(table[currentCode], stderr);
		
		previousCode = currentCode;
	}
	
	//Clean-up
	for(unsigned int i=0; i < BIT_W_MAX; i++)
		Seq_Destroy(table[i]);

	free(table);
	Bits_Destory(inputBits);
}
