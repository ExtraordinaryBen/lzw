#include "dict.h"

///
/// Constructor for Dict.
///
Dict* Dict_Create_Dictionary()
{
	Dict *dict = (Dict *)malloc(sizeof (Dict));
	dict->hashSize = MAX_HASH_SIZE;
	dict->entries = (DictNode **)calloc(dict->hashSize, sizeof (DictNode *));
	
	//Implement first 255 entries
	for(int i = 0; i < INITIAL_CODE; i++) {
		Seq *seq = Seq_Create(i);
		Dict_Insert_Pair(dict, Dict_Create_Node(seq, i), i);
		//printf("Hash of '%s': %d\n", seq->string, Seq_Compute_Hash(seq, dict->hashSize));
	}
	
	return dict;
}

///
/// Determines if the sequence s exists in the dictionary dict.
/// Returns true if found, and sets entry to the appropriate sequence.
/// Returns false if not found, entry is then set to NULL.
///
bool Dict_Find(Dict *dict, Seq *s, DictNode **entry)
{
	unsigned int index = Seq_Compute_Hash(s, dict->hashSize);
	DictNode *temp = dict->entries[index];
	
	while(temp != NULL) {
		if(Seq_Are_Equal(temp->key, s)) {
			(*entry) = temp;
			return true;
		}
		temp = temp->next;
	}
	
	(*entry) = NULL;
	return false;
}

///
/// Inserts a Seq/Code pair entry into the dictionary at a specified index. 
///
void Dict_Insert_Pair(Dict *dict, DictNode *pair, unsigned int index)
{
	assert(dict != NULL);
	if(dict->entries[index] == NULL)
		dict->entries[index] = pair;
	else {
		DictNode *temp = dict->entries[index];
	
		while(temp->next != NULL)
			temp = temp->next;
			
		//printf("Pair insert: %c, index: %u\n", Seq_First_Char(pair->key), index);
		temp->next = pair;
	}
}

///
/// Constructor for a dictionary Seq/Code pair entry.
///
DictNode* Dict_Create_Node(Seq *key, unsigned int value)
{
	DictNode *node = (DictNode *)malloc(sizeof (DictNode));
	node->code = value;
	node->next = NULL;
	node->key = key;
	
	return node;
}

///
/// Destructor for Dict.
///
void Dict_Destroy(Dict *dict)
{
	for(int i = 0; i < dict->hashSize; i++) {
		DictNode *temp = dict->entries[i];
		Dict_Destroy_Nodes(temp);
	}
	
	free(dict->entries);
	free(dict);
}

///
/// Destructor for DictNode.
///
void Dict_Destroy_Nodes(DictNode *node)
{
	if(node != NULL) {
		Dict_Destroy_Nodes(node->next);
		Seq_Destroy(node->key);
		free(node);
	}
}
